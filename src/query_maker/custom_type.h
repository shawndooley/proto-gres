


#include <string>
#include <google/protobuf/message.h>

#pragma once

namespace protogres {

// Can be used to define a custom type 
class CustomType {
  public:

    // 
    virtual std::string protobuf_type() = 0;
    virtual std::string postgres_type() = 0;

    virtual std::string format_value(const::google::protobuf::Message& message, const google::protobuf::FieldDescriptor* descriptor, int index = -1) = 0;
};

}// end of namespace protogres
