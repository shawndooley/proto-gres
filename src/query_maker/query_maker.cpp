// Copyright © 2018 Shawn Dooley
// This file is part of ProtoGres.
// ProtoGres is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ProtoGres is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ProtoGres.  If not, see <https://www.gnu.org/licenses/>.

#include "query_maker.h"
#include <numeric>
#include <sstream>
#include <google/protobuf/message.h>
#include <google/protobuf/text_format.h>
#include <algorithm>


namespace {
static void dots_to_underscore(std::string& in_string) {
  std::replace(in_string.begin(), in_string.end(), '.','_');
}

typedef std::map<std::string,std::string> MessageMap;
typedef MessageMap::iterator MapItr;
} // End of anonymous namespace.

namespace protogres {

// NOTE Does not include TYPE_MESSAGE, TYPE_GROUP, or TYPE_ENUM
std::map<google::protobuf::FieldDescriptor::Type,std::string> QueryMaker::field_descriptor_type_map_ = {
  { google::protobuf::FieldDescriptor::Type::TYPE_DOUBLE, "float8" },
  { google::protobuf::FieldDescriptor::Type::TYPE_FLOAT, "real" },
  { google::protobuf::FieldDescriptor::Type::TYPE_INT64, "bigint" },
  { google::protobuf::FieldDescriptor::Type::TYPE_UINT64, "bigint" },
  { google::protobuf::FieldDescriptor::Type::TYPE_SFIXED64, "bigint" },
  { google::protobuf::FieldDescriptor::Type::TYPE_SINT64, "bigint" },
  { google::protobuf::FieldDescriptor::Type::TYPE_FIXED64, "bigint" },
  { google::protobuf::FieldDescriptor::Type::TYPE_INT32, "integer" },
  { google::protobuf::FieldDescriptor::Type::TYPE_FIXED32, "integer" },
  { google::protobuf::FieldDescriptor::Type::TYPE_SFIXED32, "integer" },
  { google::protobuf::FieldDescriptor::Type::TYPE_SINT32, "integer" },
  { google::protobuf::FieldDescriptor::Type::TYPE_UINT32, "integer" },
  { google::protobuf::FieldDescriptor::Type::TYPE_BOOL, "boolean" },
  { google::protobuf::FieldDescriptor::Type::TYPE_STRING, "text" },
  { google::protobuf::FieldDescriptor::Type::TYPE_BYTES, "bytea" }
};

QueryMaker::FieldInfo QueryMaker::GetFieldInfo(
    const google::protobuf::FieldDescriptor* field_desc,
    const std::string scope, int extra_dimensions) {
  FieldInfo field_info;
  field_info.label = field_desc->label();
  field_info.field_name = scope + field_desc->name();
  field_info.field_type = field_descriptor_type_map_[field_desc->type()];

  field_info.dimensions = extra_dimensions;

  return field_info;
}

void QueryMaker::MakeTableDataConvenient(
    const google::protobuf::Descriptor* descriptor,
    TableInfo& table_info,
    const std::string scope /* = std::string()*/,
    const int extra_dimensions /* = 1*/) {

  std::string current_scope;
  if (scope.empty()) {
    // tname Temporary name to replace dots with underscores
    std::string tname = descriptor->full_name();
    dots_to_underscore(tname);
    table_info.name = tname;
  } else {
    current_scope = scope + "_";
  }

  std::vector<FieldInfo>& fields = table_info.fields;

  const int field_count = descriptor->field_count();
  for (int field_index = 0; field_index < field_count; ++ field_index) {

    const google::protobuf::FieldDescriptor* field_desc = descriptor->field(field_index);

    int local_dimensions = field_desc->is_repeated() ? extra_dimensions + 1 : extra_dimensions;

    switch(field_desc->type())
    {
      using namespace google::protobuf;
      case FieldDescriptor::TYPE_DOUBLE:
      case FieldDescriptor::TYPE_FIXED32:
      case FieldDescriptor::TYPE_INT32:
      case FieldDescriptor::TYPE_UINT32:
      case FieldDescriptor::TYPE_SFIXED32:
      case FieldDescriptor::TYPE_SINT32:
      case FieldDescriptor::TYPE_FLOAT:
      case FieldDescriptor::TYPE_INT64:
      case FieldDescriptor::TYPE_UINT64:
      case FieldDescriptor::TYPE_FIXED64:
      case FieldDescriptor::TYPE_SFIXED64:
      case FieldDescriptor::TYPE_SINT64:
      case FieldDescriptor::TYPE_BOOL:
      case FieldDescriptor::TYPE_STRING:
      case FieldDescriptor::TYPE_BYTES:
      {
        FieldInfo field_info = GetFieldInfo(field_desc, current_scope, local_dimensions);
        fields.push_back(field_info);
        break;
      }
      case FieldDescriptor::TYPE_GROUP:
      {
        /// TODO ???
        break;
      }
      case FieldDescriptor::TYPE_MESSAGE:
      {
        // TODO
        // add custom handler,

        // recurse otherwise.

        std::string message_scope = current_scope + field_desc->name();

        MakeTableDataConvenient(field_desc->message_type(), table_info, message_scope, local_dimensions);

        break;
      }
      case FieldDescriptor::TYPE_ENUM:
      {
        // TODO
        // This name still has a dot in it, we should only do that
        // transformation once.
        FieldInfo field_info;
        field_info.label = field_desc->label();
        field_info.field_name = current_scope + field_desc->name();

        std::string full_name = field_desc->enum_type()->full_name();
        dots_to_underscore(full_name);

        field_info.field_type = full_name;

        table_info.enums.emplace(field_desc->enum_type());
        fields.push_back(field_info);
        break;
      }
    }
  }
}

std::vector<std::string> QueryMaker::CreateTableQueries(const google::protobuf::Descriptor* descriptor) {
  std::vector<std::string> return_vector;

  TableInfo table_info;
  MakeTableDataConvenient(descriptor, table_info);

  for (const google::protobuf::EnumDescriptor* ed : table_info.enums) {
    return_vector.push_back(GenerateEnumQuery(ed));
  }
  return_vector.push_back(GenerateTableQuery(table_info));

  return return_vector;
}

std::string QueryMaker::GenerateTableQuery(const TableInfo& table_info) {
  std::stringstream ss;
  ss << "CREATE TABLE ";
  ss << table_info.name;
  ss << " ( ";

  bool is_first = true;
  for (const FieldInfo& field_info : table_info.fields) {

    if (is_first) {
      is_first = false;
    } else {
      ss << ", ";
    }
    ss << "\n ";

    ss << field_info.field_name;
    ss <<  " " ;
    ss << field_info.field_type;

    if (field_info.label == google::protobuf::FieldDescriptor::LABEL_REQUIRED) {
      ss << " NOT NULL";
    }

    // Add extra dimensions for repeated fields.
    for (int extra_dimension = 0; extra_dimension < field_info.dimensions; ++extra_dimension) {
      ss << "[]";
    }
  }
  ss << " )";

  return ss.str();
}

std::string QueryMaker::GenerateEnumQuery(const google::protobuf::EnumDescriptor* descriptor) {

  EnumData ed = MakeEnumDataConvenient(descriptor);
  std::stringstream ss;

  ss << "CREATE TYPE ";
  ss << ed.enum_name;
  ss << " AS ENUM (";

  bool is_first = true;
  for ( auto& value : ed.enum_values) {
    if (is_first) {
      is_first = false;
    } else {
      ss << ", ";
    }
    ss << "'";
    ss << value;
    ss << "'";
  }

  ss << " )";
  return ss.str();
}

std::map<std::string,std::string> QueryMaker::GetMessageInfo(const google::protobuf::Message& message, std::string scope /*= std::string()*/, const int dimensions /* = 1*/) {
  int local_dimensions = dimensions;
  // I would like to take a different approach here after reading more of
  // the protobuf documentation.

  // TODO Uggh, this approach might make repeated fields suck.
  const google::protobuf::Reflection* reflection = message.GetReflection();

  std::vector<const google::protobuf::FieldDescriptor*> descriptors;

  reflection->ListFields(message, &descriptors);

  std::map<std::string,std::string> return_message_infos;

  for (const google::protobuf::FieldDescriptor* descriptor : descriptors) {

    //NOTE in text_format.h
    // Outputs a textual representation of the value of the field supplied on
    // the message supplied. For non-repeated fields, an index of -1 must
    // be supplied. Note that this method will print the default value for a
    // field if it is not set.
    //
    // TODO Nested Fail!Nested

    std::string current_scope;
    if (scope.empty()) {
      current_scope = descriptor->name();
    } else {
      current_scope = scope + "_" + descriptor->name();
    }

    // TODO Better name
    std::string nested_scope = current_scope;

    if (descriptor->type() == google::protobuf::FieldDescriptor::TYPE_MESSAGE) {

      if (descriptor->is_repeated()) {

        // TODO Maybe the MessageInfo should be a map/multimap to start with
        int size = reflection->FieldSize(message, descriptor);
        std::vector<MessageMap> repeated_message_infos;
        for ( int count = 0 ; count < size; ++count) {

          const google::protobuf::Message* this_message = &reflection->GetRepeatedMessage(message, descriptor, count);

          repeated_message_infos.push_back( GetMessageInfo(*this_message, nested_scope, local_dimensions));
        }

        std::multimap<std::string,std::string> message_info_map;
        std::set<std::string> keys;
        for (auto& x : repeated_message_infos) {
          for (auto& y : x) {
            message_info_map.insert(y);
            keys.insert(y.first);
          }
        }

        MessageMap result_map;
        for (const auto& key : keys) {
          std::pair<MapItr,MapItr> it = message_info_map.equal_range(key);

          std::stringstream ss;
          bool first = true;
          for (MapItr i = it.first; i!= it.second; ++i) {
            if (first) {
              first = false;
              ss << "ARRAY[";
              ss <<  i->second;
            } else {
              ss << ", " << i->second;
            }
          }
          ss << "]";

          return_message_infos.insert(std::make_pair(key,ss.str()));
        }

      } else {
        const google::protobuf::Message* this_message = &reflection->GetMessage(message, descriptor);

        MessageMap nested_message_info = GetMessageInfo(*this_message, nested_scope, local_dimensions);

        for (auto& x : nested_message_info) {
          return_message_infos.insert(x);
        }
      }
    } else {
      std::string value_as_string;
      if (descriptor->is_repeated()) {
        value_as_string = GetValueForRepeatedField(message,descriptor);
      } else {
        value_as_string = GetValueForField(message,descriptor);
      }
      return_message_infos.insert(
          std::make_pair(
            nested_scope,
            value_as_string));
    }
  }

  return return_message_infos;
}

std::string QueryMaker::CreateInsertQuery(const google::protobuf::Message& message) {
  MessageMap message_infos = GetMessageInfo(message);

  std::stringstream cs_field_names;
  std::stringstream cs_values_as_strings;
  bool is_first = true;

  for (auto& message_info: message_infos) {
    if (is_first) {
      is_first = false;
    } else {
      cs_field_names <<  ", ";
      cs_values_as_strings << ", ";
    }
    cs_field_names << message_info.first << " ";
    cs_values_as_strings << message_info.second << " ";
  }

  const google::protobuf::Descriptor* message_descriptor = message.GetDescriptor();

  std::string table_name = message_descriptor->full_name();
  dots_to_underscore(table_name);

  std::stringstream ss;
  ss << "INSERT INTO ";
  ss << table_name;
  ss << "\n ( ";
  ss << cs_field_names.str();
  ss << " ) \n";
  ss << " VALUES \n ( " ;
  ss << cs_values_as_strings.str() ;
  ss << " ) ";

  return ss.str();
}

QueryMaker::EnumData QueryMaker::MakeEnumDataConvenient(const google::protobuf::EnumDescriptor* descriptor) {
  EnumData data;

  std::string table_name = descriptor->full_name();
  dots_to_underscore(table_name);
  data.enum_name = table_name;

  // Gets a value by index, where 0 <= index < value_count().
  // These are returned in the order they were defined in the .proto file.
  // const EnumValueDescriptor* value(int index) const;
  int value_count = descriptor->value_count();
  for (int  current_index = 0; current_index < value_count ; ++current_index) {
    const google::protobuf::EnumValueDescriptor* evd = descriptor->value(current_index);
    std::string full_name = table_name + "_" + evd->name();
    dots_to_underscore(full_name);
    data.enum_values.push_back(full_name);
  }

  return data;
}

std::string QueryMaker::GetValueForField(const google::protobuf::Message& message, const google::protobuf::FieldDescriptor* descriptor, int index /*= -1*/) {

  std::string value_as_string;
  if (descriptor->type() == google::protobuf::FieldDescriptor::Type::TYPE_ENUM) {
    EnumData data = MakeEnumDataConvenient(descriptor->enum_type());

    std::stringstream enum_ss;

    std::string prefix = data.enum_name;
    google::protobuf::TextFormat::PrintFieldValueToString(
        message,
        descriptor,
        index,
        &value_as_string);

    enum_ss << "'" << prefix << "_" << value_as_string <<"'";

    value_as_string = enum_ss.str();

  } else {
    google::protobuf::TextFormat::PrintFieldValueToString(
        message,
        descriptor,
        index,
        &value_as_string);
  }

  if (descriptor->type() == google::protobuf::FieldDescriptor::Type::TYPE_STRING) {
    value_as_string.front() = '\'';
    value_as_string.back() = '\'';

  } else if (descriptor->type() == google::protobuf::FieldDescriptor::Type::TYPE_BYTES) {
    // TODO I think this requires special escape characters.
    value_as_string.front() = '\'';
    value_as_string.back() = '\'';
  }

  return value_as_string;
}

std::string QueryMaker::GetValueForRepeatedField(const google::protobuf::Message& message, const google::protobuf::FieldDescriptor* descriptor) {

  std::string value_as_string;
  const google::protobuf::Reflection* reflection = message.GetReflection();
  int rep_count = reflection->FieldSize(message,descriptor);

  bool repeated_first = true;
  std::stringstream rep_ss;
  std::string string_to_pass;
  rep_ss << "ARRAY[";

  for (int i = 0; i < rep_count; i++) {

    if (repeated_first) {
      repeated_first = false;
    } else {
      rep_ss << " , ";
    }
    string_to_pass = GetValueForField( message, descriptor, i);

    rep_ss << string_to_pass;
  }
  rep_ss << "]";
  value_as_string = rep_ss.str();

  return value_as_string;
}

} // end of namespace protogres
