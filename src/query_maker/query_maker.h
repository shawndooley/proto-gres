// Copyright © 2018 Shawn Dooley
// This file is part of ProtoGres.
// ProtoGres is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ProtoGres is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ProtoGres.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <memory> 
#include <string>
#include <map>
#include <iostream>
#include <unordered_set>

#include <google/protobuf/message.h>
#include "custom_type.h"


#define asdf(x) std::cout << #x"=" << x << std::endl


namespace protogres {

class QueryMaker {
  public:

    std::vector <std::string> CreateTableQueries(const google::protobuf::Descriptor* descriptor);
    std::string CreateInsertQuery(const google::protobuf::Message& message);

    template<typename T>
    void AddCustomType(){
      std::unique_ptr<CustomType> t (new T);
      custom_type_map_.insert(
          std::make_pair(
            t->protobuf_type(),
            std::move(t)));
    }

  private: 
    struct FieldInfo {
      typedef google::protobuf::FieldDescriptor::Label Label;
      std::string field_name;
      std::string field_type;
      Label label;
      int dimensions;
    };

    struct TableInfo {
      std::string name;
      std::vector<FieldInfo> fields;
      std::unordered_set<const google::protobuf::EnumDescriptor*>  enums;

    };

    struct EnumData {
      std::string enum_name;
      std::vector<std::string> enum_values;
    };

    std::string GenerateTableQuery(const TableInfo& table_infoj);
    std::string GenerateEnumQuery(
        const google::protobuf::EnumDescriptor* descriptor);

    FieldInfo GetFieldInfo(
        const::google::protobuf::FieldDescriptor* field_desc,
        const std::string scope,
        int extra_dimensions);

    void MakeTableDataConvenient(
        const google::protobuf::Descriptor* descriptor,
        TableInfo& table_info, 
        const std::string scope = std::string(),
        const int extra_dimensions = 0
        );

    EnumData MakeEnumDataConvenient(
        const google::protobuf::EnumDescriptor* descriptor);

    std::map<std::string,std::string> GetMessageInfo(
        const google::protobuf::Message& message,
        std::string scope = std::string(),
        int dimensions = 1);

    std::map<std::string,std::string> GetRepeatedMessageInfo(
        const google::protobuf::Message& message,
        std::string scope = std::string(),
        int dimensions = 1);

    std::string GetValueForField(
        const google::protobuf::Message& message,
        const google::protobuf::FieldDescriptor* descriptor,
        int index = -1);

    std::string GetValueForRepeatedField(
        const google::protobuf::Message& message,
        const google::protobuf::FieldDescriptor* descriptor);

    static std::map<google::protobuf::FieldDescriptor::Type,std::string> field_descriptor_type_map_;

    std::map<std::string, std::unique_ptr<CustomType>> custom_type_map_;

};

} //end of namespace protogres
