// Copyright © 2018 Shawn Dooley
// This file is part of ProtoGres.
// ProtoGres is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ProtoGres is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ProtoGres.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>

#include <simple.pb.h>
#include <string>
#include "query_maker.h"

#include "custom_type.h"

#include <memory>


class CustomTimestamp: public protogres::CustomType {


  virtual std::string protobuf_type() override {
    return "timestamp";
  };

  virtual std::string postgres_type() override {
    return "timestamp with time zone";
  };



  virtual std::string format_value(const::google::protobuf::Message& message, const google::protobuf::FieldDescriptor* descriptor, int index = -1) override {

  
    return "1";
  }
};


int main(){



  SimpleTable st;
    
  protogres::QueryMaker qm;
  qm.AddCustomType<CustomTimestamp>();

  st.set_justone(1);
  st.set_just_a_string("ddddd");
  st.add_repeated_float(2.1);
  st.add_repeated_float(3.1);
  st.add_repeated_float(4.4);
  st.add_repeated_float(4.2);
  st.add_repeated_float(4.3);
  st.add_repeated_float(4.2);
  st.add_repeated_float(4.6);
  st.add_repeated_float(4.2);
  st.add_repeated_string("asd");
  st.add_repeated_string("as");


  st.mutable_look()->mutable_at()->mutable_all()->mutable_of()->mutable_these()->mutable_nested()->mutable_messages()->set_value(9);
  st.mutable_look()->mutable_at()->mutable_all()->mutable_of()->mutable_these()->mutable_nested()->mutable_messages()->add_times_two(3);
  st.mutable_look()->mutable_at()->mutable_all()->mutable_of()->mutable_these()->mutable_nested()->mutable_messages()->add_times_two(4);
  
    // TODO Make a repeated repeated field to make sure the syntax comes out okay. 
  
  //auto queries = qm.CreateTableQueries(st.descriptor());
  //for (auto& x : queries) {
    //std::cout << x << std::endl;
  //}

  std::cout << qm.CreateInsertQuery(st) << std::endl;

  return 0;
}
