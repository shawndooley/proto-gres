// Copyright © 2018 Shawn Dooley
// This file is part of ProtoGres.
// ProtoGres is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ProtoGres is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ProtoGres.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>

#include "simple.pb.h"


int main(int argc, char* argv[]){

  SimpleTable st;
  std::cout << "ot.descriptor: " << st.descriptor()->full_name() << std::endl;

  int field_count = st.descriptor()->field_count();
  for(int i=0; i< field_count; ++i)
  {
    std::cout << st.descriptor()->field(i)->name() << std::endl;
    std::cout << st.descriptor()->field(i)->full_name() << std::endl;
    std::cout << "Typename: "<< st.descriptor()->field(i)->type_name() << std::endl;
    std::cout <<"cpp typename" <<  st.descriptor()->field(i)->cpp_type_name() << std::endl;

  }

  return 0;
}
