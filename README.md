## ProtoGres

Use a protobuf message to define database schema.

Basic usage using the example protobuf found in Protobuf documentation.

```cpp

#include "example.pb.h"
#include "query_maker.h"

#include <memory>
#include <string>
#include <iostream>

int main(){
    
  protogres::QueryMaker query_maker;

  tutorial::Person person;

  const google::protobuf::Descriptor* desc = person.descriptor();

  // Returns multiple sql statements. Because if there are enum types, we need
  // to create the enums first.
  std::vector<std::string> queries = query_maker.CreateTableQueries(desc);

  for (auto& sql : queries) {
    std::cout << sql << std::endl;
  }
  // CREATE TYPE tutorial_Person_PhoneType AS ENUM ('tutorial_Person_PhoneType_MOBILE', 'tutorial_Person_PhoneType_HOME', 'tutorial_Person_PhoneType_WORK' )
  // CREATE TABLE tutorial_Person ( 
  // name text NOT NULL, 
  // id integer NOT NULL, 
  // email text, 
  // phones_number text NOT NULL[], 
  // phones_type tutorial_Person_PhoneType[] )

  person.set_name("Shawn");
  person.set_id(143);
  tutorial::Person::PhoneNumber* phone_number = person.add_phones();
  phone_number->set_type(tutorial::Person::MOBILE);
  phone_number->set_number("617-555-1212");

  tutorial::Person::PhoneNumber* phone_number2 = person.add_phones();
  phone_number2->set_type(tutorial::Person::HOME);
  phone_number2->set_number("617-555-1234");

  std::string insert = query_maker.CreateInsertQuery(person);

  std::cout << insert << std::endl;
  // Example output: 
  // INSERT INTO tutorial_Person
  // ( id , name , phones_number , phones_type  ) 
  // VALUES 
  // ( 143 , 'Shawn' , ARRAY['617-555-1212', '617-555-1234', '617-555-1234'] , ARRAY['tutorial_Person_PhoneType_MOBILE', 'tutorial_Person_PhoneType_HOME']  )
  // 
  return 0;
}
```

